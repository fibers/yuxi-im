
#ifndef __COMMON_H__
#define __COMMON_H__
#include "ImPduBase.h"

#define PROMPT		"im-client> "
#define PROMPTION fprintf(stderr, "%s", PROMPT);

typedef void (*packet_callback_t)(CImPdu* pPdu);

#endif /*defined(__COMMON_H__) */
