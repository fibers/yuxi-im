#!/bin/bash

DES_DRI=/opt/IM/im_server/im-server-1
cp -f login_server/login_server   $DES_DRI/login_server/

cp -f route_server/route_server   $DES_DRI/route_server/

cp -f msg_server/tcp_msg_server   $DES_DRI/tcp_msg_server/

cp -f http_msg_server/http_msg_server   $DES_DRI/http_msg_server/

cp -f file_server/file_server   $DES_DRI/file_server/

cp -f push_server/push_server   $DES_DRI/push_server/

cp -f db_proxy_server/db_proxy_server   $DES_DRI/db_proxy_server/

cp -f msfs/msfs   $DES_DRI/msfs/
