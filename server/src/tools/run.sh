#/bin/sh
#start or stop the im-server

FILE_SERVER=file_server
LOGIN_SERVER=login_server
MSG_SERVER=tcp_msg_server
ROUTE_SERVER=route_server
MSFS_SERVER=msfs
HTTP_MSG_SERVER=http_msg_server
PUSH_SERVER=push_server
DB_PROXY_SERVER=db_proxy_server

function restart() {
    cd $1
    if [ ! -e *.conf  ]
    then
        echo "no config file"
        return
    fi

    if [ -e server.pid  ]; then
        pid=`cat server.pid`
        echo "kill pid=$pid"
        kill $pid
        while true
        do
            oldpid=`ps -ef|grep $1|grep $pid`;
            if [ $oldpid" " == $pid" " ]; then
                echo $oldpid" "$pid
                sleep 1
            else
                break
            fi
        done
        ../daeml ./$1
    else 
        ../daeml ./$1
    fi
    cd ..
}

function stop {
    cd $1
    if [ ! -e *.conf  ]
    then
        echo "no config file"
        return
    fi

    if [ -e server.pid  ]; then
        pid=`cat server.pid`
        echo "kill pid=$pid"
        kill $pid
        while true
        do
            oldpid=`ps -ef|grep $1|grep $pid`;
            if [ $oldpid" " == $pid" " ]; then
                echo $oldpid" "$pid
                sleep 1
            else
                break
            fi
        done
     fi
     cd ..
}



restart_im_server() {
       # cd $INSTALL_DIR/$IM_SERVER
        restart $LOGIN_SERVER
        restart $ROUTE_SERVER
        restart $MSG_SERVER
        restart $FILE_SERVER
        restart $MSFS_SERVER
        restart $HTTP_MSG_SERVER
        restart $PUSH_SERVER
        restart $DB_PROXY_SERVER
}

stop_im_server() {
        #cd $INSTALL_DIR/$IM_SERVER
        stop $LOGIN_SERVER
        stop $ROUTE_SERVER
        stop $MSG_SERVER
        stop $FILE_SERVER
        stop $MSFS_SERVER
        stop $HTTP_MSG_SERVER
        stop $PUSH_SERVER
        stop $DB_PROXY_SERVER
}

case $1 in
        stop)
		stop_im_server
                ;;
        start)
                restart_im_server
                ;;
        *)
                print_help
                ;;
esac
