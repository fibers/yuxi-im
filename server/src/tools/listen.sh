#/bin/sh
#start or stop the im-server

FILE_SERVER=file_server
LOGIN_SERVER=login_server
MSG_SERVER=tcp_msg_server
ROUTE_SERVER=route_server
MSFS_SERVER=msfs
HTTP_MSG_SERVER=http_msg_server
PUSH_SERVER=push_server
DB_PROXY_SERVER=db_proxy_server

bin_dir=/opt/IM/im_server/im-server-1

function listenPid {
    cd $bin_dir/$1
    if [ ! -e *.conf  ]
    then
        echo "no config file"
        return
    fi

    if [ -e server.pid  ]
    then
        pid=`cat server.pid`
        num=`ps -ef | grep $pid | grep -v grep | wc -l`
        if [ $num -eq 0 ]                                    # 判断进程是否存在
        then
             ../daeml ./$1
        fi
        echo "num="$num
     fi
     cd ..
}


listen_im_server() {
        #cd $INSTALL_DIR/$IM_SERVER
        listenPid $LOGIN_SERVER
        listenPid $ROUTE_SERVER
        listenPid $MSG_SERVER
        listenPid $FILE_SERVER
        listenPid $MSFS_SERVER
        listenPid $HTTP_MSG_SERVER
        listenPid $PUSH_SERVER
        listenPid $DB_PROXY_SERVER
}

case $1 in
        listen)
                listen_im_server
                ;;
        *)
                print_help
                ;;
esac

