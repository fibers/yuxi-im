#!/bin/sh

#https://github.com/alexeyxo/protobuf-objc

SRC_DIR=./
DST_DIR=./ios_gen

#C++
mkdir -p $DST_DIR
#protoc -I=$SRC_DIR --cpp_out=$DST_DIR/cpp/ $SRC_DIR/*.proto
protoc --plugin=/usr/local/bin/protoc-gen-objc $SRC_DIR/*.proto --objc_out=$DST_DIR
