#ifndef MESSAGECOUNTER_H_
#define MESSAGECOUNTER_H_

#include "ImPduBase.h"
namespace DB_PROXY {

    void getUnreadMsgCounter(CImPdu* pPdu, uint32_t conn_uuid);
    void clearUnreadMsgCounter(CImPdu* pPdu, uint32_t conn_uuid);
    
    void setDevicesToken(CImPdu* pPdu, uint32_t conn_uuid);
    void getDevicesToken(CImPdu* pPdu, uint32_t conn_uuid);

    //murray add start
    void addFriendComfirmReq(CImPdu* pPdu, uint32_t conn_uuid);
    void addFriendReq(CImPdu* pPdu, uint32_t conn_uuid);

    void addGroupMemberReq(CImPdu* pPdu, uint32_t conn_uuid);
    void addGroupMemberComfirmReq(CImPdu* pPdu, uint32_t conn_uuid);

    void delFriendReq(CImPdu* pPdu, uint32_t conn_uuid);

    void getTokenAndSendPushMsg(uint32_t conn_uuid, uint32_t user_id, uint32_t to_user_id, uint32_t push_type,  uint32_t group_id = 0);
    //murray add end
};


#endif /* MESSAGECOUNTER_H_ */
