#include "../ProxyConn.h"
#include "../CachePool.h"
#include "MessageCounter.h"
#include "MessageModel.h"
#include "GroupMessageModel.h"
#include "IM.Message.pb.h"
#include "IM.BaseDefine.pb.h"
#include "IM.Login.pb.h"
#include "IM.Server.pb.h"
#include "UserModel.h"

namespace DB_PROXY {

    void getUnreadMsgCounter(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Message::IMUnreadMsgCntReq msg;
        IM::Message::IMUnreadMsgCntRsp msgResp;
        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            CImPdu* pPduResp = new CImPdu;

            uint32_t nUserId = msg.user_id();

            list<IM::BaseDefine::UnreadInfo> lsUnreadCount;
            uint32_t nTotalCnt = 0;
            
            CMessageModel::getInstance()->getUnreadMsgCount(nUserId, nTotalCnt, lsUnreadCount);
            CGroupMessageModel::getInstance()->getUnreadMsgCount(nUserId, nTotalCnt, lsUnreadCount);
            msgResp.set_user_id(nUserId);
            msgResp.set_total_cnt(nTotalCnt);
            for(auto it= lsUnreadCount.begin(); it!=lsUnreadCount.end(); ++it)
            {
                IM::BaseDefine::UnreadInfo* pInfo = msgResp.add_unreadinfo_list();
    //            *pInfo = *it;
                pInfo->set_session_id(it->session_id());
                pInfo->set_session_type(it->session_type());
                pInfo->set_unread_cnt(it->unread_cnt());
                pInfo->set_latest_msg_id(it->latest_msg_id());
                pInfo->set_latest_msg_data(it->latest_msg_data());
                pInfo->set_latest_msg_type(it->latest_msg_type());
                pInfo->set_latest_msg_from_user_id(it->latest_msg_from_user_id());
            }
            
            
            log("userId=%d, unreadCnt=%u, totalCount=%u", nUserId, msgResp.unreadinfo_list_size(), nTotalCnt);
            msgResp.set_attach_data(msg.attach_data());
            pPduResp->SetPBMsg(&msgResp);
            pPduResp->SetSeqNum(pPdu->GetSeqNum());
            pPduResp->SetServiceId(IM::BaseDefine::SID_MSG);
            pPduResp->SetCommandId(IM::BaseDefine::CID_MSG_UNREAD_CNT_RESPONSE);
            CProxyConn::AddResponsePdu(conn_uuid, pPduResp);
        }
        else
        {
            log("parse pb failed");
        }
    }

    void clearUnreadMsgCounter(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Message::IMMsgDataReadAck msg;
        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            uint32_t nUserId = msg.user_id();
            uint32_t nFromId = msg.session_id();
            IM::BaseDefine::SessionType nSessionType = msg.session_type();
            CUserModel::getInstance()->clearUserCounter(nUserId, nFromId, nSessionType);
            log("userId=%u, peerId=%u, type=%u", nFromId, nUserId, nSessionType);
        }
        else
        {
            log("parse pb failed");
        }
    }
        
    void setDevicesToken(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Login::IMDeviceTokenReq msg;
        IM::Login::IMDeviceTokenRsp msgResp;
        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            uint32_t nUserId = msg.user_id();
            string strToken = msg.device_token();
            CImPdu* pPduResp = new CImPdu;
            CacheManager* pCacheManager = CacheManager::getInstance();
            CacheConn* pCacheConn = pCacheManager->GetCacheConn("token");
            if (pCacheConn)
            {
                IM::BaseDefine::ClientType nClientType = msg.client_type();
                string strValue;
                if(IM::BaseDefine::CLIENT_TYPE_IOS == nClientType)
                {
                    strValue = "ios:"+strToken;
                }
                else if(IM::BaseDefine::CLIENT_TYPE_ANDROID == nClientType)
                {
                    strValue = "android:"+strToken;
                }
                else
                {
                    strValue = strToken;
                }
                
                string strOldValue = pCacheConn->get("device_"+int2string(nUserId));
                
                if(!strOldValue.empty())
                {
                    size_t nPos = strOldValue.find(":");
                    if(nPos!=string::npos)
                    {
                        string strOldToken = strOldValue.substr(nPos + 1);
                        string strReply = pCacheConn->get("device_"+strOldToken);
                        if (!strReply.empty()) {
                            string strNewValue("");
                            pCacheConn->set("device_"+strOldToken, strNewValue);
                        }
                    }
                }
                
                pCacheConn->set("device_"+int2string(nUserId), strValue);
                string strNewValue = int2string(nUserId);
                pCacheConn->set("device_"+strToken, strNewValue);
            
                log("setDeviceToken. userId=%u, deviceToken=%s", nUserId, strToken.c_str());
                pCacheManager->RelCacheConn(pCacheConn);
            }
            else
            {
                log("no cache connection for token");
            }
            
            log("setDeviceToken. userId=%u, deviceToken=%s", nUserId, strToken.c_str());
            msgResp.set_attach_data(msg.attach_data());
            msgResp.set_user_id(nUserId);
            pPduResp->SetPBMsg(&msgResp);
            pPduResp->SetSeqNum(pPdu->GetSeqNum());
            pPduResp->SetServiceId(IM::BaseDefine::SID_LOGIN);
            pPduResp->SetCommandId(IM::BaseDefine::CID_LOGIN_RES_DEVICETOKEN);
            CProxyConn::AddResponsePdu(conn_uuid, pPduResp);
        }
        else
        {
            log("parse pb failed");
        }
    }


    void getDevicesToken(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Server::IMGetDeviceTokenReq msg;
        IM::Server::IMGetDeviceTokenRsp msgResp;
        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            CacheManager* pCacheManager = CacheManager::getInstance();
            CacheConn* pCacheConn = pCacheManager->GetCacheConn("token");
            CImPdu* pPduResp = new CImPdu;
            uint32_t nCnt = msg.user_id_size();
            if (pCacheConn)
            {
                vector<string> vecTokens;
                for (uint32_t i=0; i<nCnt; ++i) {
                    string strKey = "device_"+int2string(msg.user_id(i));
                    vecTokens.push_back(strKey);
                }
                map<string, string> mapTokens;
                bool bRet = pCacheConn->mget(vecTokens, mapTokens);
                pCacheManager->RelCacheConn(pCacheConn);
                
                if(bRet)
                {
                    for (auto it=mapTokens.begin(); it!=mapTokens.end(); ++it) {
                        string strKey = it->first;
                        size_t nPos = strKey.find("device_");
                        if( nPos != string::npos)
                        {
                            string strUserId = strKey.substr(nPos + strlen("device_"));
                            uint32_t nUserId = string2int(strUserId);
                            string strValue = it->second;
                            nPos = strValue.find(":");
                            if(nPos!=string::npos)
                            {
                                string strType = strValue.substr(0, nPos);
                                string strToken = strValue.substr(nPos + 1);
                                IM::BaseDefine::ClientType nClientType = IM::BaseDefine::ClientType(0);
                                if(strType == "ios")
                                {
                                    nClientType = IM::BaseDefine::CLIENT_TYPE_IOS;
                                }
                                else if(strType == "android")
                                {
                                    nClientType = IM::BaseDefine::CLIENT_TYPE_ANDROID;
                                }
                                if(IM::BaseDefine::ClientType_IsValid(nClientType))
                                {
                                    IM::BaseDefine::UserTokenInfo* pToken = msgResp.add_user_token_info();
                                    pToken->set_user_id(nUserId);
                                    pToken->set_token(strToken);
                                    pToken->set_user_type(nClientType);
                                    uint32_t nTotalCnt = 0;
                                    CMessageModel::getInstance()->getUnReadCntAll(nUserId, nTotalCnt);
                                    CGroupMessageModel::getInstance()->getUnReadCntAll(nUserId, nTotalCnt);
                                    pToken->set_push_count(nTotalCnt);
                                    pToken->set_push_type(1);
                                }
                                else
                                {
                                    log("invalid clientType.clientType=%u", nClientType);
                                }
                            }
                            else
                            {
                                log("invalid value. value=%s", strValue.c_str());
                            }
                            
                        }
                        else
                        {
                            log("invalid key.key=%s", strKey.c_str());
                        }
                    }
                }
                else
                {
                    log("mget failed!");
                }
            }
            else
            {
                log("no cache connection for token");
            }
            
            log("req devices token.reqCnt=%u, resCnt=%u", nCnt, msgResp.user_token_info_size());
            
            msgResp.set_attach_data(msg.attach_data());
            pPduResp->SetPBMsg(&msgResp);
            pPduResp->SetSeqNum(pPdu->GetSeqNum());
            pPduResp->SetServiceId(IM::BaseDefine::SID_OTHER);
            pPduResp->SetCommandId(IM::BaseDefine::CID_OTHER_GET_DEVICE_TOKEN_RSP);
            CProxyConn::AddResponsePdu(conn_uuid, pPduResp);
        }
        else
        {
            log("parse pb failed");
        }
    }
	//******************************************************
	// Method:     delFriendReq
	// Access:     public
	// Returns:    void
	// Parameter:  void
	// Note:	   删除好友
	// Author      muzongcun  2015/07/07 create
	//*******************************************************
    void delFriendReq(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Message::IMMsgDelFriendReq msg;

        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            CImPdu* pPduResp = new CImPdu;

            uint32_t nUserId 		= msg.user_id();
            uint32_t nSessionId 	= msg.session_id();
            uint32_t nSessionType 	= msg.session_type();
            uint32_t nMessageId 	= msg.msg_id();
            uint32_t nNow = (uint32_t)time(NULL);
           bool bRet = CMessageModel::getInstance()->delFriendReq(nUserId, nSessionId);
        }
        else
        {
            log("parse pb failed");
        }
    }
	//******************************************************
	// Method:     getTokenAndSendPushMsg
	// Access:     public
	// Returns:    void
	// Parameter:  void
	// Note:	   得到当前用户的设备token，并发送推送
	// Author      muzongcun  2015/07/07 create
	//*******************************************************
void getTokenAndSendPushMsg(uint32_t conn_uuid, uint32_t user_id, uint32_t to_user_id, uint32_t push_type,  uint32_t group_id) {
	IM::Server::IMAddDelFriendGroupRsp msg;
	CacheManager* pCacheManager = CacheManager::getInstance();
	CacheConn* pCacheConn = pCacheManager->GetCacheConn("token");
	CImPdu* pPduResp = new CImPdu;

	if (pCacheConn) {
		vector<string> vecTokens;
		string strKey = "device_" + int2string(user_id);
		vecTokens.push_back(strKey);

		map<string, string> mapTokens;
		bool bRet = pCacheConn->mget(vecTokens, mapTokens);
		pCacheManager->RelCacheConn(pCacheConn);

		if (bRet) {
			for (auto it = mapTokens.begin(); it != mapTokens.end(); ++it) {
				string strKey = it->first;
				log("---------strKey=%s", strKey.c_str());
				size_t nPos = strKey.find("device_");
				if (nPos != string::npos) {
					string strUserId = strKey.substr(nPos + strlen("device_"));
					log("---------strUserId=%s", strUserId.c_str());
					uint32_t nUserId = string2int(strUserId);
					string strValue = it->second;
					nPos = strValue.find(":");
					if (nPos != string::npos) {
						string strType = strValue.substr(0, nPos);
						string strToken = strValue.substr(nPos + 1);
						IM::BaseDefine::ClientType nClientType = IM::BaseDefine::ClientType(0);
						if (strType == "ios") {
							nClientType = IM::BaseDefine::CLIENT_TYPE_IOS;
						} else if (strType == "android") {
							nClientType = IM::BaseDefine::CLIENT_TYPE_ANDROID;
						}
						if (IM::BaseDefine::ClientType_IsValid(nClientType)) {
							IM::BaseDefine::UserTokenInfo* pToken = msg.add_user_token_info();
							pToken->set_user_id(nUserId);
							pToken->set_token(strToken);
							pToken->set_user_type(nClientType);
							uint32_t nTotalCnt = 0;
							CMessageModel::getInstance()->getUnReadCntAll(nUserId, nTotalCnt);
							CGroupMessageModel::getInstance()->getUnReadCntAll(nUserId, nTotalCnt);
							pToken->set_push_count(nTotalCnt);
							pToken->set_push_type(1);
						} else {
							log("invalid clientType.clientType=%u", nClientType);
						}
					} else {
						log("invalid value. value=%s", strValue.c_str());
					}

				} else {
					log("invalid key.key=%s", strKey.c_str());
				}
			}
		} else {
			log("mget failed!");
		}
	} else {
		log("no cache connection for token");
	}

	log("req devices resCnt=%u", msg.user_token_info_size());


	msg.set_from_user_id(user_id);
	msg.set_to_user_id(to_user_id);
	msg.set_push_type((IM::BaseDefine::PushAddDelFriendGroupMsgType)push_type);
	msg.set_group_id(group_id);

	pPduResp->SetPBMsg(&msg);
	pPduResp->SetSeqNum(0);
	pPduResp->SetServiceId(IM::BaseDefine::SID_OTHER);
	pPduResp->SetCommandId(IM::BaseDefine::CID_ADD_DEL_FRIEND_GROUP_RSP);
	CProxyConn::AddResponsePdu(conn_uuid, pPduResp);

}

	//******************************************************
	// Method:     addFriendReq
	// Access:     public
	// Returns:    void
	// Parameter:  void
	// Note:	   添加好友
	// Author      muzongcun  2015/07/07 create
	//*******************************************************
    void addFriendReq(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Message::IMMsgAddFriendReq msg;

        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            CImPdu* pPduResp = new CImPdu;

            uint32_t nUserId 		= msg.user_id();
            uint32_t nSessionId 	= msg.session_id();
            uint32_t nSessionType 	= msg.session_type();
            uint32_t nMessageId 	= msg.msg_id();
            uint32_t nNow = (uint32_t)time(NULL);
            string msg_data = msg.msg_data();
            bool bRet = CMessageModel::getInstance()->addFriendReq(nUserId, nSessionId, nNow, 1, msg_data);

            getTokenAndSendPushMsg(conn_uuid, nUserId, nSessionId,IM::BaseDefine::ADD_FRIEND_PUSH_NOTIFY );
        }
        else
        {
            log("parse pb failed");
        }
    }
	//******************************************************
	// Method:     addFriendComfirmReq
	// Access:     public
	// Returns:    void
	// Parameter:  void
	// Note:	   添加好友确认
	// Author      muzongcun  2015/07/07 create
	//*******************************************************
    void addFriendComfirmReq(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Message::IMMsgAddFriendConfirmReq msg;

        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            CImPdu* pPduResp = new CImPdu;

            uint32_t nUserId 		= msg.user_id();
            uint32_t nSessionId 	= msg.session_id();
            uint32_t nSessionType 	= msg.session_type();
            uint32_t nMessageId 	= msg.msg_id();
            uint32_t nResult 		= msg.result();
            uint32_t nNow = (uint32_t)time(NULL);
            if (nResult == 1) {
            	bool bRet = CMessageModel::getInstance()->updateAddFriendConfirmStauts(nUserId, nSessionId,nNow,3,nResult);
            	getTokenAndSendPushMsg(conn_uuid, nUserId, nSessionId,IM::BaseDefine::ADD_FRIEND_CONFIRM_PUSH_NOTIFY );
            }else {
            	getTokenAndSendPushMsg(conn_uuid, nUserId, nSessionId,IM::BaseDefine::ADD_FRIEND_REFUSE_PUSH_NOTIFY );
            }

        }
        else
        {
            log("parse pb failed");
        }
    }

	//******************************************************
	// Method:     addGroupMemberReq
	// Access:     public
	// Returns:    void
	// Parameter:  void
	// Note:	   添加群成员请求
	// Author      muzongcun  2015/07/07 create
	//*******************************************************
    void addGroupMemberReq(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Message::IMMsgAddGroupMemberReq msg;

        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            CImPdu* pPduResp = new CImPdu;

            uint32_t nUserId 		= msg.user_id();
            uint32_t nSessionId 	= msg.session_id();
            uint32_t nSessionType 	= msg.session_type();
            uint32_t nGroupId 		= msg.group_id();
            uint32_t nMessageId 	= msg.msg_id();

            uint32_t nNow = (uint32_t)time(NULL);
            string msg_data = msg.msg_data();
            bool bRet = CMessageModel::getInstance()->addGroupMemberReq(nUserId, nSessionId, nSessionType, nGroupId,nNow,1,msg_data);
            getTokenAndSendPushMsg(conn_uuid, nUserId, nSessionId,IM::BaseDefine::ADD_GROUP_PUSH_NOTIFY ,nGroupId);
        }
        else
        {
            log("parse pb failed");
        }
    }

	//******************************************************
	// Method:     addGroupMemberComfirmReq
	// Access:     public
	// Returns:    void
	// Parameter:  void
	// Note:	   添加群成员确认请求
	// Author      muzongcun  2015/07/07 create
	//*******************************************************
    void addGroupMemberComfirmReq(CImPdu* pPdu, uint32_t conn_uuid)
    {
        IM::Message::IMMsgAddGroupMemberConfirmReq msg;

        if(msg.ParseFromArray(pPdu->GetBodyData(), pPdu->GetBodyLength()))
        {
            CImPdu* pPduResp = new CImPdu;

            uint32_t nUserId 		= msg.user_id();
            uint32_t nSessionId 	= msg.session_id();
            uint32_t nSessionType 	= msg.session_type();
            uint32_t nGroupId 		= msg.group_id();
            uint32_t nMessageId 	= msg.msg_id();
            uint32_t nResult 		= msg.result();
            uint32_t nNow = (uint32_t)time(NULL);
            bool bRet = CMessageModel::getInstance()->updateAddGroupMemberConfirmStauts(nUserId, nSessionId,nGroupId,nNow,3,nResult);
            if (nResult == 1) {
            	getTokenAndSendPushMsg(conn_uuid, nUserId, nSessionId,IM::BaseDefine::ADD_GROUP_CONFIRM_PUSH_NOTIFY ,nGroupId);
            }else {
            	getTokenAndSendPushMsg(conn_uuid, nUserId, nSessionId,IM::BaseDefine::ADD_GROUP_REFUSE_PUSH_NOTIFY ,nGroupId);
            }

        }
        else
        {
            log("parse pb failed");
        }
    }
    //murray add end

};


