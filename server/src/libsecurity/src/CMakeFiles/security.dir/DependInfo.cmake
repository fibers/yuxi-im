# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/root/git_im/TeamTalk/server/src/libsecurity/src/aes_core.cpp" "/root/git_im/TeamTalk/server/src/libsecurity/src/CMakeFiles/security.dir/aes_core.cpp.o"
  "/root/git_im/TeamTalk/server/src/libsecurity/src/base64.cpp" "/root/git_im/TeamTalk/server/src/libsecurity/src/CMakeFiles/security.dir/base64.cpp.o"
  "/root/git_im/TeamTalk/server/src/libsecurity/src/md5.cpp" "/root/git_im/TeamTalk/server/src/libsecurity/src/CMakeFiles/security.dir/md5.cpp.o"
  "/root/git_im/TeamTalk/server/src/libsecurity/src/security.cpp" "/root/git_im/TeamTalk/server/src/libsecurity/src/CMakeFiles/security.dir/security.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "AC_HAS_CRITICAL"
  "AC_HAS_DEBUG"
  "AC_HAS_ERROR"
  "AC_HAS_INFO"
  "AC_HAS_WARNING"
  "LINUX_DAEMON"
  "SERVER"
  "TIXML_USE_STL"
  "_FILE_OFFSET_BITS=64"
  "_REENTRANT"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "../include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
